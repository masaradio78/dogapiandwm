package com.example.docpictures.web;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class BreedControllerIT {

  @Autowired
  MockMvc mockMvc;

  @RegisterExtension
  static WireMockExtension wireMockServer = WireMockExtension.newInstance()
      .options(wireMockConfig().dynamicPort())
      .build();
  private final static String PATH_ALL_BREEDS = "/breeds/list/all";

  @DynamicPropertySource
  static void configureProperties(DynamicPropertyRegistry registry) {
    registry.add("dog.api-base", wireMockServer::baseUrl);
  }

  @Test
  void should_find_all_breeds() throws Exception {
    wireMockServer.stubFor(WireMock.get(WireMock.urlMatching(PATH_ALL_BREEDS))
        .willReturn(
            aResponse()
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .withBody("""
                    {
                      "message": {
                        "affenpinscher": []
                      }
                    }
                    """)
        )
    );
    this.mockMvc.perform(get("/api/v1/breeds"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[0].name").value("affenpinscher"))
        .andExpect(jsonPath("$.[0].subBreedList", Matchers.hasSize(0)));
  }

  @Test
  void should_find_all_breeds_with_subBreeds() throws Exception {
    wireMockServer.stubFor(WireMock.get(WireMock.urlMatching(PATH_ALL_BREEDS))
        .willReturn(
            aResponse()
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .withBody("""
                    {
                      "message": {
                        "affenpinscher": [],
                        "australian": [
                          "shepherd"
                        ],
                        "boxer": [],
                        "hound": [
                            "afghan",
                            "basset",
                            "blood"
                        ]
                      }
                    }
                    """)
        )
    );
    this.mockMvc.perform(get("/api/v1/breeds"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[0].name").value("affenpinscher"))
        .andExpect(jsonPath("$.[0].subBreedList", Matchers.hasSize(0)))
        .andExpect(jsonPath("$.[1].name").value("australian"))
        .andExpect(jsonPath("$.[1].subBreedList", Matchers.contains("shepherd")))
        .andExpect(jsonPath("$.[2].name").value("boxer"))
        .andExpect(jsonPath("$.[2].subBreedList", Matchers.hasSize(0)))
        .andExpect(jsonPath("$.[3].name").value("hound"))
        .andExpect(jsonPath("$.[3].subBreedList", Matchers.contains("afghan", "basset", "blood")));
  }
}