package com.example.docpictures.bdd.steps;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Etantdonné;
import io.cucumber.java.fr.Lorsque;
import org.hamcrest.Matchers;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BreedsSteps {
  Map<String, List<String>> listBreeds = new HashMap<>();
  private final static String PATH_ALL_BREEDS = "/breeds/list/all";
  private final MockMvc mockMvc;
  private ResultActions response;

  public BreedsSteps(MockMvc mockMvc) {
    this.mockMvc = mockMvc;
    WireMock.configureFor("dog.ceo/api", 443);
  }

  @Etantdonné("la race {string} dans la liste des races de chiens")
  public void laRaceDansLaListeDesRacesDeChiens(String breed) {

    listBreeds.put(breed, List.of());
  }

  @Lorsque("l'utilisateur souhaite voir la liste des races de chiens")
  public void lUtilisateurSouhaiteVoirLaListeDesRacesDeChiens() throws Exception {
    var objectMapper = new ObjectMapper();
    var listBreedsJson = objectMapper.writeValueAsString(listBreeds);

    stubFor(WireMock.get(WireMock.urlMatching(PATH_ALL_BREEDS))
        .willReturn(
            aResponse()
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .withBody("""
                    {
                      "message": %s
                    }
                    """.formatted(listBreedsJson))
        )
    );

     response = mockMvc.perform(get("/api/v1/breeds"));
  }


  @Alors("il devrait voir la race {string} dans la liste")
  public void ilDevraitVoirLaRaceDansLaListe(String breed) throws Exception {
    response.andExpect(status().isOk())
        .andExpect(jsonPath("$.[0].name").value("affenpinscher"))
        .andExpect(jsonPath("$.[0].subBreedList", Matchers.hasSize(0)));
  }
}
