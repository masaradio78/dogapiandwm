# language: fr

Fonctionnalité:
  En tant qu'utilisateur
  Je souhaite afficher la race des chiens


  Scénario: Afficher la liste des chiens avec ses sous races
    Etant donné la race "affenpinscher" dans la liste des races de chiens
    Lorsque l'utilisateur souhaite voir la liste des races de chiens
    Alors il devrait voir la race "affenpinscher" dans la liste
#
#  Scénario: Afficher la liste des chiens avec ses sous races
#    Etant donné la race "affenpinscher" dans la liste des races de chiens
#    Et la race "australian" dans la liste des races de chiens avec comme sous races
#    |shepherd|
#    Et la race "boxer" dans la liste des races de chiens
#    Et la race "hound" dans la liste des races de chiens avec comme sous races
#    |afghan|basset|blood|