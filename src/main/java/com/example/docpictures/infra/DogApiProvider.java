package com.example.docpictures.infra;

import com.google.common.net.HttpHeaders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class DogApiProvider {
  private final WebClient webClient;
  private final String dogApiBaseUrl;

  public DogApiProvider(WebClient webClient, @Value("${dog.api-base}") String dogApiBaseUrl) {
    this.webClient = webClient;
    this.dogApiBaseUrl = dogApiBaseUrl;
  }

  public AllBreeds getAllBreeds() {
    return webClient.get()
        .uri(dogApiBaseUrl + "/breeds/list/all")
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .retrieve()
        .bodyToMono(AllBreeds.class)
        .block();
  }
}
