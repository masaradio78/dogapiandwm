package com.example.docpictures.infra;

import java.util.List;
import java.util.Map;

public record AllBreeds(Map<String, List<String>> message, String status) {
}
