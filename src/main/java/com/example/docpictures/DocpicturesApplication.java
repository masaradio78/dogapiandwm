package com.example.docpictures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class DocpicturesApplication {

  public static void main(String[] args) {
    SpringApplication.run(DocpicturesApplication.class, args);
  }

  @Bean
  WebClient webClient() {
    return WebClient.create();
  }
}
