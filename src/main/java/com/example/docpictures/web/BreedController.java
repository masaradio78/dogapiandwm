package com.example.docpictures.web;

import com.example.docpictures.core.Breed;
import com.example.docpictures.infra.DogApiProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/breeds")
public class BreedController {
  private final DogApiProvider docApiProvider;

  public BreedController(DogApiProvider docApiProvider) {
    this.docApiProvider = docApiProvider;
  }

  @GetMapping()
  List<Breed> findAll() {
    return docApiProvider.getAllBreeds().message().entrySet()
        .stream()
        .map((entry) -> new Breed(entry.getKey(), entry.getValue()))
        .toList();
  }
}
