package com.example.docpictures.core;

import java.util.List;

public interface BreedRepository {
  List<Breed> findAll();
}
