package com.example.docpictures.core;

import java.util.List;

public record Breed(String name, List<String> subBreedList) {
}
